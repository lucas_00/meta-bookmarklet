import Vue from 'vue';
import App from './App.vue';

import { CardPlugin, OverlayPlugin, ModalPlugin, ButtonPlugin, BadgePlugin, TooltipPlugin, LinkPlugin, TabsPlugin, ImagePlugin} from 'bootstrap-vue'
Vue.use(CardPlugin)
Vue.use(OverlayPlugin)
Vue.use(ModalPlugin)
Vue.use(ButtonPlugin)
Vue.use(BadgePlugin)
Vue.use(TooltipPlugin)
Vue.use(LinkPlugin)
Vue.use(TabsPlugin)
Vue.use(ImagePlugin)

import { BIcon, BIconXCircleFill, BIconCheckCircleFill, BIconExclamationCircleFill } from 'bootstrap-vue'

Vue.component('BIcon', BIcon)
Vue.component('BIconXCircleFill', BIconXCircleFill)
Vue.component('BIconCheckCircleFill', BIconCheckCircleFill)
Vue.component('BIconExclamationCircleFill', BIconExclamationCircleFill)

import './assets/scss/_variables.scss'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

