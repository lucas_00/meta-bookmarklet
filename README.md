# meta-bookmarklet

## Description
Bookmarklet con Vue JS / Revela los meta del sitio visitado.

## Preview
Click [aqui](https://goofy-khorana-864ca7.netlify.app/) para preview.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
